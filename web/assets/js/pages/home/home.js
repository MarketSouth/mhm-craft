//Nav Items
    let navLine = document.querySelector(".navLine");
    let linkBox = document.querySelector(".linkBox");

    navLine.style.maxHeight = 0;
    linkBox.addEventListener("mouseout", () => {
      navLine.style.maxHeight = 0;
    });
    $(".navigation-box").hover(function() {

      if ($(this).index() == 0) {
        $(".navLine").css("max-height", "14.5rem");
      } else {
        let defaultHeight = 14.5;
        let actualHeight = ($(this).index() * 3.6) + defaultHeight;
        $(".navLine").css("max-height", actualHeight + "rem");
      }
      if ($(".navigationImage").find('img').length > 0 && $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').length > 0) {
        $(".navigationImage").find('img').hide();
        $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').show();
      } else {
        $(".navigationImage").find('img').hide();
        $(".navigationImage").find('img:nth-child(0)').show();
      }
    });


//Menu Cross
     var menu = document.querySelector("#menu");
     var menuBox = document.querySelector("#menuBox");
     var menuBar = document.querySelector("#menuBar");
     menuBar.classList.add("translate-x-full");
     menuBox.addEventListener("click", () => {
       menu.classList.toggle("cross");
       menuBar.classList.toggle("translate-x-full");
     });
