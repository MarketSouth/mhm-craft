//Menu Cross
var menu = document.querySelector("#menu");
var menuBox = document.querySelector("#menuBox");
var menuBar = document.querySelector("#menuBar");
var cWriting = document.querySelector("#copywriting");

menuBar.classList.add("translate-x-full");
menuBox.addEventListener("click", () => {
  menu.classList.toggle("cross");
  menuBar.classList.toggle("translate-x-full");
});


//Dynamic
let mL = document.querySelectorAll(".menuLines");
  let mTC = document.querySelector(".menuTextChange");
  let nLC = document.querySelector(".navLogoChange");
  let nLMC = document.querySelector(".navLogoMobileChange");
  let mLine = document.querySelector(".mobileLine");
  let main = document.querySelector("main");
  let nav = document.querySelector(".nav");

  main.addEventListener("mouseover", () => {
    nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";

    mL.forEach((m) => {
      m.classList.remove("bg-white");
      m.classList.add("bg-navy");
    });
    mTC.classList.remove("text-white");
    mTC.classList.add("text-navy");
    nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
    mLine.classList.remove("border-white");
    mLine.classList.add("border-navy");
    nav.classList.add("border-transparent");
    nav.classList.remove("border-white");
    cWriting.classList.remove("text-white");
    cWriting.classList.add("text-navy");
  });

  menuBar.addEventListener("mouseover", () => {
    mL.forEach((m) => {
      m.classList.add("bg-white");
      m.classList.remove("bg-navy");
    });
    mTC.classList.add("text-white");
    mTC.classList.remove("text-navy");
    nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
    nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
    mLine.classList.remove("border-navy");
    mLine.classList.add("border-white");
    nav.classList.remove("border-transparent");
    nav.classList.add("border-white");
    cWriting.classList.add("text-white");
    cWriting.classList.remove("text-navy");
  });
  nav.addEventListener("mouseover", () => {
    mL.forEach((m) => {
      m.classList.add("bg-white");
      m.classList.remove("bg-navy");
    });
    mTC.classList.add("text-white");
    mTC.classList.remove("text-navy");
    nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
    nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
    mLine.classList.remove("border-navy");
    mLine.classList.add("border-white");
    nav.classList.remove("border-transparent");
    nav.classList.add("border-white");
    cWriting.classList.add("text-white");
    cWriting.classList.remove("text-navy");
  });


//Contact Movement
let info = document.querySelector("#info");
let form = document.querySelector("#form");

function iShow() {
  info.classList.remove("hideI");
  info.classList.remove("showF");
  info.classList.add("showI");
  form.classList.add("hideF");
}

function fShow() {
  info.classList.remove("showI");
  form.classList.remove("hideF");
  info.classList.add("hideI");
  form.classList.add("showF");
}

//Nav Items
         let navLine = document.querySelector(".navLine");
         let linkBox = document.querySelector(".linkBox");

         navLine.style.maxHeight = 0;
         linkBox.addEventListener("mouseout", () => {
           navLine.style.maxHeight = 0;
         });
         $(".navigation-box").hover(function() {

           if ($(this).index() == 0) {
             $(".navLine").css("max-height", "14.5rem");
           } else {
             let defaultHeight = 14.5;
             let actualHeight = ($(this).index() * 3.6) + defaultHeight;
             $(".navLine").css("max-height", actualHeight + "rem");
           }
           if ($(".navigationImage").find('img').length > 0 && $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').length > 0) {
             $(".navigationImage").find('img').hide();
             $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').show();
           } else {
             $(".navigationImage").find('img').hide();
             $(".navigationImage").find('img:nth-child(0)').show();
           }
         });
