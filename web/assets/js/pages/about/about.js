//Menu Cross
      var menu = document.querySelector("#menu");
      var menuBox = document.querySelector("#menuBox");
      var menuBar = document.querySelector("#menuBar");
      var cWriting = document.querySelector("#copywriting");

      menuBar.classList.add("translate-x-full");
      menuBox.addEventListener("click", () => {
       menu.classList.toggle("cross");
       menuBar.classList.toggle("translate-x-full");
      });

      //Dynamic Nav

           var mL = document.querySelectorAll(".menuLines");
           var mTC = document.querySelector(".menuTextChange");
           var nLC = document.querySelector(".navLogoChange");
           var nLMC = document.querySelector(".navLogoMobileChange");
           var main = document.querySelector("main");
           var nav = document.querySelector(".nav");
           var toTop = document.querySelector(".backToTop");

           function navLG() {
             if (y.matches) {
               window.addEventListener("scroll", () => {
                 if (window.pageYOffset >= 1150) {
                   nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
                   cWriting.classList.remove("opacity-0");
                 } else {
                   nLC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                   cWriting.classList.add("opacity-0");
                 }
                 if (window.pageYOffset >= 1400) {
                   mL.forEach((m) => {
                     m.classList.remove("bg-white");
                     m.classList.add("bg-navy");
                   });
                   mTC.classList.remove("text-white");
                   mTC.classList.add("text-navy");
                   nLMC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
                 } else {
                   mL.forEach((m) => {
                     m.classList.add("bg-white");
                     m.classList.remove("bg-navy");
                   });
                   mTC.classList.add("text-white");
                   mTC.classList.remove("text-navy");
                   nLMC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 }
                 nav.classList.add("border-transparent");
                 nav.classList.remove("border-white");
               });

               main.addEventListener("mouseover", () => {
                 if (window.pageYOffset >= 850) {
                   nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
                   cWriting.classList.remove("opacity-0");
                 } else {
                   nLC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                   cWriting.classList.add("opacity-0");
                 }
                 if (window.pageYOffset >= 1400) {
                   mL.forEach((m) => {
                     m.classList.remove("bg-white");
                     m.classList.add("bg-navy");
                   });
                   mTC.classList.remove("text-white");
                   mTC.classList.add("text-navy");
                   nLMC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
                 } else {
                   mL.forEach((m) => {
                     m.classList.add("bg-white");
                     m.classList.remove("bg-navy");
                   });
                   mTC.classList.add("text-white");
                   mTC.classList.remove("text-navy");
                   nLMC.src =
                     "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 }
                 nav.classList.add("border-transparent");
                 nav.classList.remove("border-white");
                 cWriting.classList.remove("text-white");
                 cWriting.classList.add("text-navy");
               });

               menuBar.addEventListener("mouseover", () => {
                 mL.forEach((m) => {
                   m.classList.add("bg-white");
                   m.classList.remove("bg-navy");
                 });
                 mTC.classList.add("text-white");
                 mTC.classList.remove("text-navy");
                 nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 nav.classList.remove("border-transparent");
                 nav.classList.add("border-white");
                 cWriting.classList.add("text-white");
                 cWriting.classList.remove("text-navy");
               });

               nav.addEventListener("mouseover", () => {
                 mL.forEach((m) => {
                   m.classList.add("bg-white");
                   m.classList.remove("bg-navy");
                 });
                 mTC.classList.add("text-white");
                 mTC.classList.remove("text-navy");
                 nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
                 nav.classList.remove("border-transparent");
                 nav.classList.add("border-white");
                 cWriting.classList.add("text-white");
                 cWriting.classList.remove("text-navy");
               });
             }
           }

     let y = window.matchMedia("(min-width: 1024px)");
     navLG(y); // Call listener function at run time
     y.addListener(navLG); // Attach listener function on state changes


//Back To Top
       function backToTop() {
         toTop.classList.add("opacity-0");
         toTop.classList.add("pointer-events-none");
         window.addEventListener("scroll", () => {
           if (window.pageYOffset >= 1200) {
             toTop.classList.remove("opacity-0");
             toTop.classList.add("opacity-100");
             toTop.classList.remove("pointer-events-none");
           } else {
             toTop.classList.add("opacity-0");
             toTop.classList.remove("opacity-100");
             toTop.classList.add("pointer-events-none");
           }
         });
       }

       backToTop();


//Scrolling Animation

   gsap.registerPlugin(ScrollTrigger);

   function gsapMD() {
     if (x.matches) {
       // If media query matches
       ScrollTrigger.refresh();

       //Company History
       ScrollTrigger.create({
         trigger: "#history",
         start: "top top",
         end: "bottom 450px",
         pin: "#historyText",
       });
       //Story
       ScrollTrigger.create({
         trigger: "#story",
         start: "top top",
         end: "bottom bottom",
         pin: "#storyImage",
       });
       //HC
       ScrollTrigger.create({
         trigger: "#HCExpand",
         start: "top top",
         end: "bottom bottom",
         pin: "#HCExpandImage",
       });
       //Mercer
       ScrollTrigger.create({
         trigger: "#mercerExpand",
         start: "top top",
         end: "bottom bottom",
         pin: "#mercerExpandImage",
       });
       //Sclave
       ScrollTrigger.create({
         trigger: "#sclaveExpand",
         start: "top top",
         end: "bottom bottom",
         pin: "#sclaveExpandImage",
       });
     }
   }

   let x = window.matchMedia("(min-width: 768px)");
   gsapMD(x); // Call listener function at run time
   x.addListener(gsapMD); // Attach listener function on state changes

//Show Hide for H&C, Mercer & S-Clav

   let companies = document.querySelectorAll("[class*=Company_]");
   let arrows = document.querySelectorAll("[class*='Arrow_']");

   console.log(arrows);
   $("[class*=Company_]").addClass("hidden");
   function onCompanyClick(companyNumber) {
     if($("[class*=Company_"+companyNumber+"]").hasClass("hidden")) {
         $("[class*=Company_]").addClass("hidden");
         $("[class*=Company_"+companyNumber+"]").removeClass("hidden");

         ScrollTrigger.refresh();
        $("[class*=Company_"+companyNumber+"]")[0].scrollIntoView();
        $("[class*=Arrow_]").removeClass("-rotate-45");
        $("[class*=Arrow_]").addClass("rotate-45");
        $("[class*=Arrow_"+companyNumber+"]").removeClass("rotate-45");
        $("[class*=Arrow_"+companyNumber+"]").addClass("-rotate-45");
      }
      else {
        $("[class*=Company_"+companyNumber+"]").addClass("hidden");
        $("[class*=Arrow_"+companyNumber+"]").removeClass("-rotate-45");
        $("[class*=Arrow_"+companyNumber+"]").addClass("rotate-45");
      }
   }

//Values Show-Hide

   let values = document.querySelectorAll("[id^=values]");
   let vContent = document.querySelector("#_valuesContent");

   console.log(values);
   $("[id^=values]").addClass("hidden");
   function onValueClick(valueNumber) {
     if($("[id^=values"+valueNumber+"]").hasClass("hidden")) {
       $("[id^=values]").addClass("hidden");
       $("[id^=values"+valueNumber+"]").removeClass("hidden");
       ScrollTrigger.refresh();
       //vContent.scrollIntoView();
       $("#"+valueNumber+"_valuesContent")[0].scrollIntoView();
    }
    else {
      $("[id^=values"+valueNumber+"]").addClass("hidden");
      $("#our-values-block")[0].scrollIntoView();
    }
   }
