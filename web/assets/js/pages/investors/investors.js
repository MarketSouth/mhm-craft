//Menu Cross
var menu = document.querySelector("#menu");
var menuBox = document.querySelector("#menuBox");
var menuBar = document.querySelector("#menuBar");
var cWriting = document.querySelector("#copywriting");

menuBar.classList.add("translate-x-full");
menuBox.addEventListener("click", () => {
  menu.classList.toggle("cross");
  menuBar.classList.toggle("translate-x-full");
});



//Dynamic Nav

var mL = document.querySelectorAll(".menuLines");
var mTC = document.querySelector(".menuTextChange");
var nLC = document.querySelector(".navLogoChange");
var nLMC = document.querySelector(".navLogoMobileChange");
var main = document.querySelector("main");
var nav = document.querySelector(".nav");
var toTop = document.querySelector(".backToTop");

var mL = document.querySelectorAll(".menuLines");
      var mTC = document.querySelector(".menuTextChange");
      var nLC = document.querySelector(".navLogoChange");
      var nLMC = document.querySelector(".navLogoMobileChange");
      var mLine = document.querySelector(".mobileLine");
      var main = document.querySelector("main");
      var nav = document.querySelector(".nav");
      var toTop = document.querySelector(".backToTop");

      function navLG() {
        if (y.matches) {
          window.addEventListener("scroll", () => {
            if (window.pageYOffset >= 200) {
              nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
              cWriting.classList.remove("opacity-0");
            } else {
              nLC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
              cWriting.classList.add("opacity-0");
            }
            if (window.pageYOffset >= 700) {
              mL.forEach((m) => {
                m.classList.remove("bg-white");
                m.classList.add("bg-navy");
              });
              mTC.classList.remove("text-white");
              mTC.classList.add("text-navy");
              nLMC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
            } else {
              mL.forEach((m) => {
                m.classList.add("bg-white");
                m.classList.remove("bg-navy");
              });
              mTC.classList.add("text-white");
              mTC.classList.remove("text-navy");
              nLMC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            }
            nav.classList.add("border-transparent");
            nav.classList.remove("border-white");
          });

          main.addEventListener("mouseover", () => {
            if (window.pageYOffset >= 200) {
              nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
              cWriting.classList.remove("opacity-0");
            } else {
              nLC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
              cWriting.classList.add("opacity-0");
            }
            if (window.pageYOffset >= 700) {
              mL.forEach((m) => {
                m.classList.remove("bg-white");
                m.classList.add("bg-navy");
              });
              mTC.classList.remove("text-white");
              mTC.classList.add("text-navy");
              nLMC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
            } else {
              mL.forEach((m) => {
                m.classList.add("bg-white");
                m.classList.remove("bg-navy");
              });
              mTC.classList.add("text-white");
              mTC.classList.remove("text-navy");
              nLMC.src =
                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            }
            nav.classList.add("border-transparent");
            nav.classList.remove("border-white");
            cWriting.classList.remove("text-white");
            cWriting.classList.add("text-navy");
          });

          menuBar.addEventListener("mouseover", () => {
            mL.forEach((m) => {
              m.classList.add("bg-white");
              m.classList.remove("bg-navy");
            });
            mTC.classList.add("text-white");
            mTC.classList.remove("text-navy");
            nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            nav.classList.remove("border-transparent");
            nav.classList.add("border-white");
            cWriting.classList.add("text-white");
            cWriting.classList.remove("text-navy");
          });

          nav.addEventListener("mouseover", () => {
            mL.forEach((m) => {
              m.classList.add("bg-white");
              m.classList.remove("bg-navy");
            });
            mTC.classList.add("text-white");
            mTC.classList.remove("text-navy");
            nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
            nav.classList.remove("border-transparent");
            nav.classList.add("border-white");
            cWriting.classList.add("text-white");
            cWriting.classList.remove("text-navy");
          });
        }
      }

      let y = window.matchMedia("(min-width: 1024px)");
      navLG(y); // Call listener function at run time
      y.addListener(navLG); // Attach listener function on state changes



//Back To Top
function backToTop() {
  toTop.classList.add("opacity-0");
  toTop.classList.add("pointer-events-none");
  window.addEventListener("scroll", () => {
    if (window.pageYOffset >= 700) {
      toTop.classList.remove("opacity-0");
      toTop.classList.add("opacity-100");
      toTop.classList.remove("pointer-events-none");
    } else {
      toTop.classList.add("opacity-0");
      toTop.classList.remove("opacity-100");
      toTop.classList.add("pointer-events-none");
    }
  });
}

backToTop();






//Selected Button

let all = document.querySelector('.all');
all.classList.add('selectedBtn');
//  let investorsCentre = document.querySelectorAll('.btn-investorsCentre');

$('.btn-investorsCentre').on("click", function() {

  let cat = $(this).attr('data-category');
  //alert(cat);
  $(".investory-data").hide();
  $("." + cat).show();
  if ($("." + cat).length < 1) {
    $(".no-category").show();
  } else {
    $(".no-category").hide();
  }
  $('.btn-investorsCentre').removeClass("selectedBtn");
  $(this).addClass("selectedBtn");

});
