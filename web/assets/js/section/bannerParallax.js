//Banner Animation
   let bImage = document.querySelector(".bannerImage");
   let bText = document.querySelector(".bannerText");

   window.addEventListener("scroll", () => {
     let yOff = window.pageYOffset;
     bImage.style.transform = "translateY(" + yOff * 0.5 + "px)";
     bText.style.transform = "translateY(" + yOff * -0.5 + "px)";
   });
