//Nav Items
    let navLine = document.querySelector(".navLine");
    let linkBox = document.querySelector(".linkBox");

    navLine.style.maxHeight = 0;
    linkBox.addEventListener("mouseout", () => {
      navLine.style.maxHeight = 0;
    });
    $(".navigation-box").hover(function() {

      if ($(this).index() == 0) {
        $(".navLine").css("max-height", "14.5rem");
      } else {
        let defaultHeight = 14.5;
        let actualHeight = ($(this).index() * 3.6) + defaultHeight;
        $(".navLine").css("max-height", actualHeight + "rem");
      }
      if ($(".navigationImage").find('img').length > 0 && $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').length > 0) {
        $(".navigationImage").find('img').hide();
        $(".navigationImage").find('img:nth-child(' + ($(this).index() + 1) + ')').show();
      } else {
        $(".navigationImage").find('img').hide();
        $(".navigationImage").find('img:nth-child(0)').show();
      }
    });

    $(".navigation-box").click(function(e){
      setTimeout(function(){ 
		if(window.location.hash) {
			e.preventDefault();
			$("#menu").click();
			var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
			$("#"+hash).click();
			$('html, body').animate({
				 scrollTop: $("#"+hash+'-c').offset().top
			 }, 2000)
		}
	  }, 1000);
    })

    if(window.location.hash) {
      var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
        $("#"+hash).click();
      $('html, body').animate({
           scrollTop: $("#"+hash+'-c').offset().top
       }, 2000)
    }

// //Menu Cross
//       var menu = document.querySelector("#menu");
//       var menuBox = document.querySelector("#menuBox");
//       var menuBar = document.querySelector("#menuBar");
//       var cWriting = document.querySelector("#copywriting");
//
//       menuBar.classList.add("translate-x-full");
//       menuBox.addEventListener("click", () => {
//        menu.classList.toggle("cross");
//        menuBar.classList.toggle("translate-x-full");
//       });
//
// //Dynamic Nav
//
//      var mL = document.querySelectorAll(".menuLines");
//      var mTC = document.querySelector(".menuTextChange");
//      var nLC = document.querySelector(".navLogoChange");
//      var nLMC = document.querySelector(".navLogoMobileChange");
//      var mLine = document.querySelector(".mobileLine");
//      var main = document.querySelector("main");
//      var nav = document.querySelector(".nav");
//      var toTop = document.querySelector(".backToTop");
//
//      function navLG() {
//        if (y.matches) {
//          window.addEventListener("scroll", () => {
//            if (window.pageYOffset >= 850) {
//              nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
//              cWriting.classList.remove("opacity-0");
//            } else {
//              nLC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//              cWriting.classList.add("opacity-0");
//            }
//            if (window.pageYOffset >= 1400) {
//              mL.forEach((m) => {
//                m.classList.remove("bg-white");
//                m.classList.add("bg-navy");
//              });
//              mTC.classList.remove("text-white");
//              mTC.classList.add("text-navy");
//              nLMC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
//              mLine.classList.remove("border-white");
//              mLine.classList.add("border-navy");
//            } else {
//              mL.forEach((m) => {
//                m.classList.add("bg-white");
//                m.classList.remove("bg-navy");
//              });
//              mTC.classList.add("text-white");
//              mTC.classList.remove("text-navy");
//              nLMC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//              mLine.classList.remove("border-navy");
//              mLine.classList.add("border-white");
//            }
//            nav.classList.add("border-transparent");
//            nav.classList.remove("border-white");
//          });
//
//          main.addEventListener("mouseover", () => {
//            if (window.pageYOffset >= 850) {
//              nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
//              cWriting.classList.remove("opacity-0");
//            } else {
//              nLC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//              cWriting.classList.add("opacity-0");
//            }
//            if (window.pageYOffset >= 1400) {
//              mL.forEach((m) => {
//                m.classList.remove("bg-white");
//                m.classList.add("bg-navy");
//              });
//              mTC.classList.remove("text-white");
//              mTC.classList.add("text-navy");
//              nLMC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_PRIMARY.svg";
//              mLine.classList.remove("border-white");
//              mLine.classList.add("border-navy");
//            } else {
//              mL.forEach((m) => {
//                m.classList.add("bg-white");
//                m.classList.remove("bg-navy");
//              });
//              mTC.classList.add("text-white");
//              mTC.classList.remove("text-navy");
//              nLMC.src =
//                "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//              mLine.classList.remove("border-navy");
//              mLine.classList.add("border-white");
//            }
//            nav.classList.add("border-transparent");
//            nav.classList.remove("border-white");
//            cWriting.classList.remove("text-white");
//            cWriting.classList.add("text-navy");
//          });
//
//          menuBar.addEventListener("mouseover", () => {
//            mL.forEach((m) => {
//              m.classList.add("bg-white");
//              m.classList.remove("bg-navy");
//            });
//            mTC.classList.add("text-white");
//            mTC.classList.remove("text-navy");
//            nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//            nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//            mLine.classList.remove("border-navy");
//            mLine.classList.add("border-white");
//            nav.classList.remove("border-transparent");
//            nav.classList.add("border-white");
//            cWriting.classList.add("text-white");
//            cWriting.classList.remove("text-navy");
//          });
//
//          nav.addEventListener("mouseover", () => {
//            mL.forEach((m) => {
//              m.classList.add("bg-white");
//              m.classList.remove("bg-navy");
//            });
//            mTC.classList.add("text-white");
//            mTC.classList.remove("text-navy");
//            nLMC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//            nLC.src = "/assets/images/logo/MHM_Automation_Logo_Stacked_REVERSED.svg";
//            mLine.classList.remove("border-navy");
//            mLine.classList.add("border-white");
//            nav.classList.remove("border-transparent");
//            nav.classList.add("border-white");
//            cWriting.classList.add("text-white");
//            cWriting.classList.remove("text-navy");
//          });
//        }
//      }
//
//      let y = window.matchMedia("(min-width: 1024px)");
//      navLG(y); // Call listener function at run time
//      y.addListener(navLG); // Attach listener function on state changes
//
//
// //Back To Top
//        function backToTop() {
//          toTop.classList.add("opacity-0");
//          toTop.classList.add("pointer-events-none");
//          window.addEventListener("scroll", () => {
//            if (window.pageYOffset >= 1200) {
//              toTop.classList.remove("opacity-0");
//              toTop.classList.add("opacity-100");
//              toTop.classList.remove("pointer-events-none");
//            } else {
//              toTop.classList.add("opacity-0");
//              toTop.classList.remove("opacity-100");
//              toTop.classList.add("pointer-events-none");
//            }
//          });
//        }
//
//        backToTop();
