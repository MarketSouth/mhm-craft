<?php // vtipogxxoqdis
/**
 * @link http://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license http://craftcms.com/license
 */

namespace craft\behaviors;

use yii\base\Behavior;

/**
 * Custom field behavior
 *
 * This class provides attributes for all the unique custom field handles.
 *
 * @method static bannerImage(mixed $value) Sets the [[bannerImage]] property
 * @method static richText(mixed $value) Sets the [[richText]] property
 * @method static pageIntroduction(mixed $value) Sets the [[pageIntroduction]] property
 * @method static mainHeading(mixed $value) Sets the [[mainHeading]] property
 * @method static iconBlocks(mixed $value) Sets the [[iconBlocks]] property
 * @method static brandLogo(mixed $value) Sets the [[brandLogo]] property
 * @method static linkit(mixed $value) Sets the [[linkit]] property
 * @method static businessUnits(mixed $value) Sets the [[businessUnits]] property
 * @method static businessUnitExcerpt(mixed $value) Sets the [[businessUnitExcerpt]] property
 * @method static businessUnitDescription(mixed $value) Sets the [[businessUnitDescription]] property
 * @method static businessUnitLogo(mixed $value) Sets the [[businessUnitLogo]] property
 * @method static companyHistory(mixed $value) Sets the [[companyHistory]] property
 * @method static companyHistorySectionTitle(mixed $value) Sets the [[companyHistorySectionTitle]] property
 * @method static companyHistoryExcerpt(mixed $value) Sets the [[companyHistoryExcerpt]] property
 * @method static companyTimeline(mixed $value) Sets the [[companyTimeline]] property
 * @method static companyHistoryRichtext(mixed $value) Sets the [[companyHistoryRichtext]] property
 * @method static companyYear(mixed $value) Sets the [[companyYear]] property
 * @method static ourVision(mixed $value) Sets the [[ourVision]] property
 * @method static ourVisionRichtext(mixed $value) Sets the [[ourVisionRichtext]] property
 * @method static ourVisionHeading(mixed $value) Sets the [[ourVisionHeading]] property
 * @method static contactUs(mixed $value) Sets the [[contactUs]] property
 * @method static address(mixed $value) Sets the [[address]] property
 * @method static companytitle(mixed $value) Sets the [[companytitle]] property
 * @method static website(mixed $value) Sets the [[website]] property
 * @method static phone(mixed $value) Sets the [[phone]] property
 * @method static ourStory(mixed $value) Sets the [[ourStory]] property
 * @method static ourStoryImage(mixed $value) Sets the [[ourStoryImage]] property
 * @method static ourStoryHeading(mixed $value) Sets the [[ourStoryHeading]] property
 * @method static ourStoryRichtext(mixed $value) Sets the [[ourStoryRichtext]] property
 * @method static ourValues(mixed $value) Sets the [[ourValues]] property
 * @method static OurValuesContent(mixed $value) Sets the [[OurValuesContent]] property
 * @method static OurValuesTitle(mixed $value) Sets the [[OurValuesTitle]] property
 * @method static ourValueIcons(mixed $value) Sets the [[ourValueIcons]] property
 * @method static ourValueBoldTitle(mixed $value) Sets the [[ourValueBoldTitle]] property
 * @method static investorcategory(mixed $value) Sets the [[investorcategory]] property
 * @method static heading(mixed $value) Sets the [[heading]] property
 * @method static excerpt(mixed $value) Sets the [[excerpt]] property
 * @method static investorsRichText(mixed $value) Sets the [[investorsRichText]] property
 * @method static featureImage(mixed $value) Sets the [[featureImage]] property
 * @method static document(mixed $value) Sets the [[document]] property
 * @method static uploadDocument(mixed $value) Sets the [[uploadDocument]] property
 * @method static documentName(mixed $value) Sets the [[documentName]] property
 * @method static businessUnitName(mixed $value) Sets the [[businessUnitName]] property
 * @method static gallery(mixed $value) Sets the [[gallery]] property
 * @method static galleryImages(mixed $value) Sets the [[galleryImages]] property
 * @method static caption(mixed $value) Sets the [[caption]] property
 * @method static image(mixed $value) Sets the [[image]] property
 * @method static seo(mixed $value) Sets the [[seo]] property
 */
class CustomFieldBehavior extends Behavior
{
    /**
     * @var bool Whether the behavior should provide methods based on the field handles.
     */
    public $hasMethods = false;

    /**
     * @var string[] List of supported field handles.
     */
    public static $fieldHandles = [
        'bannerImage' => true,
        'richText' => true,
        'pageIntroduction' => true,
        'mainHeading' => true,
        'iconBlocks' => true,
        'brandLogo' => true,
        'linkit' => true,
        'businessUnits' => true,
        'businessUnitExcerpt' => true,
        'businessUnitDescription' => true,
        'businessUnitLogo' => true,
        'companyHistory' => true,
        'companyHistorySectionTitle' => true,
        'companyHistoryExcerpt' => true,
        'companyTimeline' => true,
        'companyHistoryRichtext' => true,
        'companyYear' => true,
        'ourVision' => true,
        'ourVisionRichtext' => true,
        'ourVisionHeading' => true,
        'contactUs' => true,
        'address' => true,
        'companytitle' => true,
        'website' => true,
        'phone' => true,
        'ourStory' => true,
        'ourStoryImage' => true,
        'ourStoryHeading' => true,
        'ourStoryRichtext' => true,
        'ourValues' => true,
        'OurValuesContent' => true,
        'OurValuesTitle' => true,
        'ourValueIcons' => true,
        'ourValueBoldTitle' => true,
        'investorcategory' => true,
        'heading' => true,
        'excerpt' => true,
        'investorsRichText' => true,
        'featureImage' => true,
        'document' => true,
        'uploadDocument' => true,
        'documentName' => true,
        'businessUnitName' => true,
        'gallery' => true,
        'galleryImages' => true,
        'caption' => true,
        'image' => true,
        'seo' => true,
    ];

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “bannerImage”.
     */
    public $bannerImage;

    /**
     * @var mixed Value for field with the handle “richText”.
     */
    public $richText;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “pageIntroduction”.
     */
    public $pageIntroduction;

    /**
     * @var string|null Value for field with the handle “mainHeading”.
     */
    public $mainHeading;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “iconBlocks”.
     */
    public $iconBlocks;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “brandLogo”.
     */
    public $brandLogo;

    /**
     * @var mixed Value for field with the handle “linkit”.
     */
    public $linkit;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “businessUnits”.
     */
    public $businessUnits;

    /**
     * @var string|null Value for field with the handle “businessUnitExcerpt”.
     */
    public $businessUnitExcerpt;

    /**
     * @var mixed Value for field with the handle “businessUnitDescription”.
     */
    public $businessUnitDescription;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “businessUnitLogo”.
     */
    public $businessUnitLogo;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “companyHistory”.
     */
    public $companyHistory;

    /**
     * @var string|null Value for field with the handle “companyHistorySectionTitle”.
     */
    public $companyHistorySectionTitle;

    /**
     * @var string|null Value for field with the handle “companyHistoryExcerpt”.
     */
    public $companyHistoryExcerpt;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “companyTimeline”.
     */
    public $companyTimeline;

    /**
     * @var mixed Value for field with the handle “companyHistoryRichtext”.
     */
    public $companyHistoryRichtext;

    /**
     * @var string|null Value for field with the handle “companyYear”.
     */
    public $companyYear;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “ourVision”.
     */
    public $ourVision;

    /**
     * @var mixed Value for field with the handle “ourVisionRichtext”.
     */
    public $ourVisionRichtext;

    /**
     * @var string|null Value for field with the handle “ourVisionHeading”.
     */
    public $ourVisionHeading;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “contactUs”.
     */
    public $contactUs;

    /**
     * @var string|null Value for field with the handle “address”.
     */
    public $address;

    /**
     * @var string|null Value for field with the handle “companytitle”.
     */
    public $companytitle;

    /**
     * @var string|null Value for field with the handle “website”.
     */
    public $website;

    /**
     * @var mixed Value for field with the handle “phone”.
     */
    public $phone;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “ourStory”.
     */
    public $ourStory;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “ourStoryImage”.
     */
    public $ourStoryImage;

    /**
     * @var string|null Value for field with the handle “ourStoryHeading”.
     */
    public $ourStoryHeading;

    /**
     * @var mixed Value for field with the handle “ourStoryRichtext”.
     */
    public $ourStoryRichtext;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “ourValues”.
     */
    public $ourValues;

    /**
     * @var mixed Value for field with the handle “OurValuesContent”.
     */
    public $OurValuesContent;

    /**
     * @var string|null Value for field with the handle “OurValuesTitle”.
     */
    public $OurValuesTitle;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “ourValueIcons”.
     */
    public $ourValueIcons;

    /**
     * @var string|null Value for field with the handle “ourValueBoldTitle”.
     */
    public $ourValueBoldTitle;

    /**
     * @var \craft\elements\db\CategoryQuery Value for field with the handle “investorcategory”.
     */
    public $investorcategory;

    /**
     * @var string|null Value for field with the handle “heading”.
     */
    public $heading;

    /**
     * @var string|null Value for field with the handle “excerpt”.
     */
    public $excerpt;

    /**
     * @var mixed Value for field with the handle “investorsRichText”.
     */
    public $investorsRichText;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “featureImage”.
     */
    public $featureImage;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “document”.
     */
    public $document;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “uploadDocument”.
     */
    public $uploadDocument;

    /**
     * @var string|null Value for field with the handle “documentName”.
     */
    public $documentName;

    /**
     * @var string|null Value for field with the handle “businessUnitName”.
     */
    public $businessUnitName;

    /**
     * @var \craft\elements\db\MatrixBlockQuery Value for field with the handle “gallery”.
     */
    public $gallery;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “galleryImages”.
     */
    public $galleryImages;

    /**
     * @var string|null Value for field with the handle “caption”.
     */
    public $caption;

    /**
     * @var \craft\elements\db\AssetQuery Value for field with the handle “image”.
     */
    public $image;

    /**
     * @var mixed Value for field with the handle “seo”.
     */
    public $seo;

    /**
     * @var array Additional custom field values we don’t know about yet.
     */
    private $_customFieldValues = [];

    /**
     * @inheritdoc
     */
    public function __call($name, $params)
    {
        if ($this->hasMethods && isset(self::$fieldHandles[$name]) && count($params) === 1) {
            $this->$name = $params[0];
            return $this->owner;
        }
        return parent::__call($name, $params);
    }

    /**
     * @inheritdoc
     */
    public function hasMethod($name)
    {
        if ($this->hasMethods && isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::hasMethod($name);
    }

    /**
     * @inheritdoc
     */
    public function __isset($name)
    {
        if (isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::__isset($name);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (isset(self::$fieldHandles[$name])) {
            return $this->_customFieldValues[$name] ?? null;
        }
        return parent::__get($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (isset(self::$fieldHandles[$name])) {
            $this->_customFieldValues[$name] = $value;
            return;
        }
        parent::__set($name, $value);
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if ($checkVars && isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        if ($checkVars && isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }
}
